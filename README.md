## Systemd Timer Notify

It provides desktop notifications when a systemd service starts and continues running.
The notification will automatically close when the service finishes.


### Screenshots

Here is an example:

![image](screenshots/1.jpg)

### Requirements

* Install `systemd`
* Install `python`
* Install `openssh`
* Install `libnotify` or `dunst` for desktop notifications

### Installation

* For Arch Linux users, the package can be installed from the [AUR](https://aur.archlinux.org/packages/systemd-timer-notify/).


### Configuration file

By default, it loads `~/.config/systemd-timer-notify.conf` or `~/.config/systemd-timer-notify/local.conf`.

If unavailable, it falls back to the default configuration file at `/etc/systemd-timer-notify.conf`.

If you want to create a custom configuration file anywhere (e.g.,`/home/user/custom.conf`), run a command line:
```
$ systemd-timer-notify -c <CONFIG_PATH>

### For example:
$ systemd-timer-notify -c "/home/user/custom.conf"
```
You can run multiple `systemd-timer-notify` processes, each using a different configuration.

To view active services triggered by timers, run:
```
$ systemctl --all list-timers`
```

Example output:
```
Sat 2022-12-24 15:00:00 CET 16min left          -                           -                  snapper-timeline.timer           snapper-timeline.service
Sun 2022-12-25 00:00:00 CET 9h left             Sat 2022-12-24 09:00:32 CET 5h 43min ago       logrotate.timer                  logrotate.service
Sun 2022-12-25 00:00:00 CET 9h left             Sat 2022-12-24 09:00:32 CET 5h 43min ago       shadow.timer                     shadow.service
Sun 2022-12-25 00:00:24 CET 9h left             Sat 2022-12-24 09:38:29 CET 5h 5min ago        btrbk.timer                      btrbk.service
Sun 2022-12-25 00:59:02 CET 10h left            Sat 2022-12-24 09:20:39 CET 5h 23min ago       man-db.timer                     man-db.service

```

### Configuration Options

* Ignore specific timers:

If you do not want to receive notifications for certain systemd timers, add them to `ignore_timers`
```
ignore_timers=shadow.timer  logrotate  man-db
```


* Set a custom notification icon:
```
icon=/usr/share/icons/breeze-dark/status/24/showinfo.svg
```

* Customize the notification message:
```
message=Please wait until this notification closes automatically when the process is done!
```

* Enable or disable remote monitoring via SSH: (yes|no)
```
enable_remote=no
```

* Configure the remote host:
  - This requires `enable_remote=yes`
  - Set up a public and private key pair without a password for SSH. Ensure **systemd** is installed on the remote host/server.

```
remote_host=$USER_NAME@$SERVER_NAME
### or
remote_host=USER@127.0.0.1
### or
remote_host=USER@fe80::1111:2222:3333:4444%eth0
```

* Description for the remote host notification:
  - If left empty, the `remote_host` value will be used
```
description=Your server name
```

### Autostart

To enable automatic startup:
```
$ cp /usr/share/applications/systemd-timer-notify.desktop ~/.config/autostart/
```

If using a custom configuration file located in a path of your choice, modify the `Exec` line in the autostart desktop file `~/.config/autostart/systemd-timer-notify.desktop`.
```
Exec=/usr/bin/systemd-timer-notify -c <CONFIG_PATH>
```


### known issues

* **KDE notification conflicts:**

If notifications are replaced by “knopwob dunst” or an unattractive version, restore the default KDE notification:

```
$ mkdir -p ~/.local/share/dbus-1/services/
$ ln -s /usr/share/dbus-1/services/org.kde.plasma.Notifications.service \
  ~/.local/share/dbus-1/services/org.kde.plasma.Notifications.service
```

Or remove the conflicting service file:

```
$ sudo rm /usr/share/dbus-1/services/org.knopwob.dunst.service
```

Or use `libnotify` instead of `dunst`

### Donate

This project is free to use, but donations are appreciated!

[<img src="https://gitlab.com/Zesko/resources/-/raw/main/kofiButton.png" width=110px>](https://ko-fi.com/zeskotron) &nbsp;[<img src="https://gitlab.com/Zesko/resources/-/raw/main/PayPalButton.png" width=160px/>](https://www.paypal.com/donate/?hosted_button_id=XKP36D62AY5HY)
