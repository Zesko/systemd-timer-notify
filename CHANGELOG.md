# Changelog

## 1.3.0 - 2025-02-23

### Added

- Support for `notify-send`.

### Changed

- Replaced `dunstify` with `notify-send` if dunst is not installed.


## 1.2.0 - 2024-12-25

### Added

- Support for loading default config files: `~/.config/systemd-timer-notify.conf` or `~/.config/systemd-timer-notify/local.conf`.
- Added logging functionality for better traceability.

### Changed

- Simplified the `load_config()` function by reducing code complexity.

## 1.1.0 - 2024-12-11

### Removed

- Removed unnecessary `/usr/share/doc/`, now the PKGBUILD handles copying documentation files to the appropriate directory for Arch Linux.


## 1.0.1 - 2024-11-20

### Fixed

- Removed `/usr/bin/` binary path for compatibility with non-FHS (Filesystem Hierarchy Standard) systems.
- Changed the default shebang to `#!/usr/bin/env bash` for improved compatibility.

## 1.0.0 - 2024-07-02

### Added

- Initial release.
